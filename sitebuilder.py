#!/usr/bin/env python

import sys
from dateutil import parser
import calendar

from flask import Flask, render_template
from flask_flatpages import FlatPages
from flask_frozen import Freezer
from highlight import *
from settings import *

from gears.environment import Environment
from gears.finders import FileSystemFinder
from gears_clean_css import CleanCSSCompressor
from gears_uglifyjs import UglifyJSCompressor

app = Flask(__name__)
pages = FlatPages(app)
app.config.from_pyfile('settings.py')

pages.init_app(app)
freezer = Freezer(app)

@app.route('/')
def index():
    for page in pages:
        pdate = parser.parse(page.meta['date'])
        page.pdate = {}
        page.pdate['month_name'] = calendar.month_name[pdate.month]
        page.pdate['month_abbr'] = calendar.month_abbr[pdate.month]
        page.pdate['month_name'] = calendar.month_name[pdate.month]
        page.pdate['month_end'] = page.pdate['month_name'][len(page.pdate['month_abbr']):]
        page.pdate['day_abbr'] = calendar.day_abbr[calendar.weekday(pdate.year, pdate.month, pdate.day)]
        page.pdate['day_name'] = calendar.day_name[calendar.weekday(pdate.year, pdate.month, pdate.day)]
        page.pdate['day'] = pdate.day
        page.pdate['year'] = pdate.year
        if page.html.find('<!-- more -->') > 0:
            page.preview = page.html[0:page.html.find('<!-- more -->')]
    return render_template('index.html', pages=sorted(pages, key=lambda p: p.meta["date"], reverse=True))

@app.route('/tag/<string:tag>/')
def tag(tag):
    tagged = [p for p in pages if tag in p.meta.get('tags', [])]
    return render_template('tag.html', pages=tagged, tag=tag)

@app.route('/<path:path>/')
def page(path):
    page = pages.get_or_404(path)
    pdate = parser.parse(page.meta['date'])
    page.pdate = {}
    page.pdate['month_name'] = calendar.month_name[pdate.month]
    page.pdate['month_abbr'] = calendar.month_abbr[pdate.month]
    page.pdate['month_name'] = calendar.month_name[pdate.month]
    page.pdate['month_end'] = page.pdate['month_name'][len(page.pdate['month_abbr']):]
    page.pdate['day_abbr'] = calendar.day_abbr[calendar.weekday(pdate.year, pdate.month, pdate.day)]
    page.pdate['day_name'] = calendar.day_name[calendar.weekday(pdate.year, pdate.month, pdate.day)]
    page.pdate['day'] = pdate.day
    page.pdate['year'] = pdate.year
    return render_template('page.html', page=page)

@app.route('/pygments.css')
def pygments_css():
    return pygments_style_defs('tango'), 200, {'Content-Type': 'text/css'}

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == "build":
        env = Environment(STATIC_DIR, public_assets=('\.css$', '\.js$'))
        env.register_defaults()
        env.finders.register(FileSystemFinder([ASSETS_DIR]))
        env.compressors.register('text/css', CleanCSSCompressor.as_handler())
        env.compressors.register('application/javascript', UglifyJSCompressor.as_handler())
        freezer.freeze()
        env.save()
    else:
        app.run(host='127.0.0.1', port=8000)
