def pygmented_markdown(text):
    """Render Markdown text to HTML. Uses the `Codehilite`_ extension
    if `Pygments`_ is available.

    .. _Codehilite: http://www.freewisdom.org/projects/python-markdown/CodeHilite
    .. _Pygments: http://pygments.org/
    """
    try:
        import pygments
    except ImportError:
        extensions = []
    else:
        extensions = ['codehilite']
    return markdown.markdown(text, extensions)

def pygments_style_defs(style='default'):
    """:return: the CSS definitions for the `Codehilite`_ Markdown plugin.

    :param style: The Pygments `style`_ to use.

    Only available if `Pygments`_ is.

    .. _Codehilite: http://www.freewisdom.org/projects/python-markdown/CodeHilite
    .. _Pygments: http://pygments.org/
    .. _style: http://pygments.org/docs/styles/
    """
    import pygments.formatters
    formater = pygments.formatters.HtmlFormatter(style=style)
    return formater.get_style_defs('.codehilite')