title: "Juniper SRX to Cisco ASA ipsec vpn"
date: 2013-01-27 23:58
tags:  [VPN, Noc, Juniper, Cisco]

В продолжение темы ipsec vpn хочется написать памятку по спариванию SRX и ASA по ipsec. Все описывать не буду, так как конфиг Джуна из [предыдущего поста про ipsec](/2012-12-20-ipsec-racoon-to-juniper-srx/) вполне подойдет.

<!-- more -->

Приведу лишь пимер конфигурации Cisco:


    crypto ipsec ikev1 transform-set srx-ph2-set esp-3des esp-sha-hmac 
    crypto map srxmap 10 match address srx
    crypto map srxmap 10 set pfs 
    crypto map srxmap 10 set peer srx-ext-ip
    crypto map srxmap 10 set ikev1 transform-set srx-ph2-set
    crypto map srxmap 10 set nat-t-disable
    crypto map srxmap 10 set reverse-route
    crypto map srxmap interface outside
    crypto isakmp identity address 
    crypto ikev2 policy 1
     encryption 3des
     integrity sha
     group 2
     prf sha
     lifetime seconds 43200
    crypto ikev1 enable outside
    crypto ikev1 enable outside-backup
    crypto ikev1 policy 130
     authentication pre-share
     encryption 3des
     hash sha
     group 2
     lifetime 86400

Вот так оно и должно работать.