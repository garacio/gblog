title: "Ipsec racoon to Juniper SRX"
date: 2012-12-20 02:12
tags: [Noc, VPN, Juniper, Unix]

На настройку этой связки, несколько месяцев назад я убил кучу времени, пишу сюда что бы не забыть...

Чуть позже опишу процесс связки Cisco ASA - Juniper SRX ipsec vpn

<!-- more -->

На стороне Ubuntu

    apt-get install racoon


    # cat /etc/default/racoon 
    CONFIG_MODE="direct"
    RACOON_ARGS=" -l /var/log/racoon.log"

    # cat /etc/racoon/racoon.conf

    log debug;
    path pre_shared_key "/etc/racoon/psk.txt";
    path certificate "/etc/racoon/certs";

    listen {
    	isakmp <linux ext ip>[500]; # установка прослушивания даемона ip[port]
    	isakmp_natt <linux ext ip>[4500]; # установка прослушивания используя NAT-T
    	strict_address;
    }

    remote <SRX ext ip> {
    	exchange_mode main;

     	proposal {
    	    encryption_algorithm 3des;
            hash_algorithm sha1;
            authentication_method pre_shared_key;
            dh_group modp1024;
    	}
    	generate_policy off;
    }

    sainfo address <Linux int network> any address <SRX int network> any {
    	pfs_group modp1024;
    	encryption_algorithm aes256;
    	authentication_algorithm hmac_sha1;
    	compression_algorithm deflate;
    }

    # cat /etc/racoon/psk.txt 

    <SRX ext ip> any-long-key

    # cat /etc/ipsec-tools.conf 

    flush; #сбросить ассоциации безопасности (SAD)
    spdflush; # сбросить политики безопасности (SPD)

    spdadd <Linux int network> <SRX int network> any -P out ipsec
       esp/tunnel/<linux ext ip>-<SRX ext ip>/require;
                         
    spdadd <SRX int network>  <Linux int network> any -P in ipsec
       esp/tunnel/<SRX ext ip>-<linux ext ip>/require;


    # service setkey restart
    # service racoon restart

На стороне Juniper SRX


    # show security ike

    proposal nomad-proposals {
        authentication-method pre-shared-keys;
        dh-group group2;
        authentication-algorithm sha1;
        encryption-algorithm 3des-cbc;
    }

    policy ike-nomad-policy {
        mode main;
        proposals nomad-proposals;
        pre-shared-key ascii-text "$9$wR2ZUkqf3/CevX-dwoaAtu0BRyrvXx-Hq/t0BhcwY2oaUzF69Cpk.nCApIR"; ## SECRET-DATA
    }

    gateway ike-gate1 {
        ike-policy ike-nomad-policy;
        address <linux ext ip>;
        external-interface ge-0/0/1.0;
    }


    # show security ipsec

    proposal nomad-ipsec-proposal {
        protocol esp;
        authentication-algorithm hmac-sha1-96;
        encryption-algorithm aes-256-cbc;
    }

    policy nomad-policy {
        perfect-forward-secrecy {
            keys group2;
        }
        proposals nomad-ipsec-proposal;
    }

    vpn nomad-vpn {
        bind-interface st0.0;
        ike {
            gateway ike-gate1;
            proxy-identity {
                local <SRX int network>;
                remote <linux int network>;
                service any;
            }
            ipsec-policy nomad-policy;
        }
    }

    # show interfaces st0.0 

    family inet {
        address 10.33.200.1/32; # Эта строчка не обязательна -  IP можно не присваивать
    }


    # show security zones security-zone vpn                   

    host-inbound-traffic {
        system-services {
            all;
        }
        protocols {
            all;
        }
    }
    interfaces {
        st0.0 {
            host-inbound-traffic {
                system-services {
                    all;
                }
                protocols {
                    all;
                }
            }
        }
    }


    # show routing-options 

    static {
        route <linux int network> next-hop st0.0;
    }

Для интерфейса st0.0 не обязательно задавать адрес, он просто должен быть

Далее политики безопастности настраиваются стандартным способом через security policy