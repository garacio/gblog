title: "Pythos static blog"
date: 2013-04-02 22:55
tags: [Python]

Этот пост - опровержение [предыдущего](/2012-12-20-octopress-i-ispachkannyie-ruki/)

Как оказалось на Python можно сделать просто замечательный статический блог движок.

Примером тому служит данный сайт сгенерированный 70 строчками python кода. Код движка можно посмотреть [тут](https://bitbucket.org/garacio/gblog/overview)