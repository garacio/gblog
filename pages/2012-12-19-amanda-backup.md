title: "Amanda backup"
date: 2012-12-19 23:43
tags: [Amanda, Backup, Unix]

Резервное копирование неотъемлемая часть любой сети чей администратор заботится о сохранности данных.
Это пост - шпаргалка для меня как настраивать эту систему резервного копирования, может и еще кому пригодится.

<!-- more -->

### Настройка сервера
 
#### Основной файл конфигурации
    
    #cat /etc/amanda/DailySet1/amanda.conf

    org "some org"
    inparallel 5
    mailto "admin@somecorp.tld"
    dumpuser "backup"
    dumporder "ssSSS"
    taperalgo   largestfit
    displayunit "m"
    dumpcycle 7
    runspercycle 7
    tapecycle 10
    tpchanger "chg-disk" # /usr/lib/amanda/chg-disk
    tapedev "file:/home/backup"
    tapetype DISK
    labelstr "DailySet1-.*"
    infofile "/etc/amanda/DailySet1/curinfo"
    logdir "/var/log/amanda/DailySet1"
    indexdir "/etc/amanda/DailySet1/index"

    define tapetype DISK {
        length 500000 MB
    }

    define dumptype mine-gnutar-root {
        comment "Dkr's description"
        compress client fast
        estimate calcsize
        priority high
        program "GNUTAR"
        strategy standard
        index yes
    }

    define dumptype mine-gnutar-var {
        ostrovok-gnutar-root
        exclude list "/var/.amandaexclude"
    }

    define dumptype mine-gnutar-lxc {
        ostrovok-gnutar-root
        exclude list "/var/lib/lxc/.amandaexclude"
    }


Создаем структуру каталогов и файлов которые потребуются во время работы.

    # mkdir /etc/amanda/DailySet1/{curinfo,index}

Создаем два пустых файла


    # touch /etc/amanda/DailySet1 /{tapelist,disk list}


Нашему проекту назначаем правильного владельца


    # chown -R backup:backup /etc/amanda/DailySet1/
    echo 'some-server-name /etc comp-tar' > /etc/amanda/DailySet1/disklist


Разрешаем пользователю root с сервера som-server-name получать с нас бекап.


    # echo 'some-server-name root' > `grep backup /etc/passwd | cut -d: -f6`/.amandahos

Создать и разметить все 65 кассет под использование амандой (загрузку/выгрузку нужных кассет аманда производит сама, если кассета уже была маркирована чужой меткой, 
то нужно писать добавить ключик force: ... amlabel -f ... ):


    # su - backup

    $ cd /home/backup
    $ for I in {1..65}; do mkdir slot$I; done

    $  for I in {1..65}; do amlabel DailySet1 DailySet1-$I slot $I; done
    Reading label...
    Found an empty tape.
    Writing label 'DailySet1-1'...
    Checking label...
    Success!
    Reading label...
    Found an empty tape.
    Writing label 'DailySet1-2'...
    Checking label...
    Success!


Посмотреть все-ли готово к следующему бэкапу:
 
    $ amcheck DailySet1

Сделать принудительно следующий бэкап (всего что описано в disklist; конкретной машины; конкретного каталога на конкретной машине):


    $ amdump DailySet1
     
    $ amdump DailySet1 some-server-name
    $ amdump DailySet1 some-server-name /var/local


одновременно в другой консоли на этой же машине можно смотреть в реалтайме статус бэкапирования:

    $ watch -n1'amstatus DailySet1' 

Иногда нужно, на следующем запуске бэкапа, какую-то машину или каталог на ней или все машины сбэкапить full-dump (то есть принудительно включить бэкап 0го уровня).
Для включения 0го уровня бэкапа для всей машины :

 
    $ amadmin DailySet1 force some-server-name

    amadmin: some-server-name:/ is set to a forced level0at next run.
    amadmin: some-server-name:/var is set to a forced level0at next run.
    amadmin: some-server-name:/var/local is set to a forced level0at next run. 

 
Если нужно включить только определенную точку монтирования, то добавляем /var/local в конец указанной строки.
Если нужно все машины переключить на 0ой уровень, то вместо some-server-name пишем '*' (звездочка в одинарных кавычках, чтобы баш звездочку не развернул).
Далее просто запускаем amdump и amstatus смотрим течение процесса.

Описание точек бэкапа в disklist:

Точка бэкапа это точка монтирования fs на сервере. То есть: /, /var, /var/local итп. При этом бэкап идет ТОЛЬКО по fs данной точки монтирования не затрагивая другие fs, даже если они примонтированы вложено.

Например, рассмотрим /var, /var/local. При бэкапе /var, точка /var/local сбэкаплена НЕ будет. Ее нужно описывать отдельно.

Итак, типичное описание бэкапа сервера в disklist:


    $ cat /etc/amanda/DailySet1/disklist | grep 
     
    some-server-name / ostrovok-gnutar-root
    some-server-name /var ostrovok-gnutar-var
    some-server-name /var/lib/lxc ostrovok-gnutar-lxc
    some-server-name /home ostrovok-gnutar-home

То есть, указываем сервер, точку монтирования fs и методику бэкапа.

Методика бэкапа описывается в /etc/amanda/DailySet1/amanda.conf, пример:


    define dumptype mine-gnutar-root {
        comment "Mine description"
        allow-split yes
        compress client best
        estimate calcsize
        #exclude list "/etc/amandaexclude"
    #    maxdumps1
        priority high
        program "GNUTAR"
        strategy standard
        index yes
    }
     
    define dumptype mine-gnutar-var {
            mine-gnutar-root
            exclude list "/var/.amandaexclude"
    }
     
    define dumptype ostrovok-gnutar-lxc {
            ostrovok-gnutar-root
            exclude list "/var/lib/lxc/.amandaexclude"
    }
    define dumptype mine-gnutar-home {
            ostrovok-gnutar-root
            exclude list "/home/.amandaexclude"
    }


Первое описание глобальное, сюда можно внести все что будет общим для всех методик бэкапа, второе уже более специализированное. Методики бэкапа различаются расположением файла.amandaexclude, файл 
в котором описываются ИСКЛЮЧЕНИЯ из списка бэкапа, то есть те каталоги, которые бэкапить не нужно. Например, исключим в /var/lib подкаталог /var/lib/postgresql из процесса бэкапа:

    some-server-name:/var/lib# cat .amandaexclude
    ./postgresql/*
 
так же явные кандидаты на исключение из процесса бэкапа: /dev, /proc, /tmp, /sys (ессно в методике mine-gnutar-root).


Можно добавить указание конкретной fs на данном хосте, например /var/local, и посмотреть только его. Так же как можно вместо хоста написать '*' и увидеть информацию обо всем что было вообще сбэкаплено амандой когда-либо.
Восстановление данных из бэкапа:

Идем на целевой хост, который хочется восстановить из бэкапа, пусть это будет для примера some-server-name
Создаем конфиг-файл клиента восстановления (он одинаков для всех клиентов):


    # mkdir /etc/amanda
    # cat /etc/amanda/amanda-client.conf
    conf "DailySet1"                # your config name
    index_server "backup.domain.tld"       # your amindexd server
    tape_server  "backup.domain.tld"       # your amidxtaped server
    auth "bsd"

запускаем amrecover (всегда под root-ом)


    $ amadmin DailySet1 find some-server-name
     
     
    root@some-server-name:~# amrecover
    AMRECOVER Version 3.2.1. Contacting server on backup.domain.tld ...
    220 bck AMANDA index server (3.3.0) ready.
    Setting restore date to today (2012-08-22)
    200 Working date set to 2012-08-22.
    200 Config set to DailySet1.
    200 Dump host set to some-server-name.
    Use the setdisk
     
    amrecover> listdisk
    200- List of disk for host some-server-name
    201- /
    200 List of disk for host some-server-name
     
    amrecover> setdisk /
    200 Disk set to /.
     
    amrecover> ls
    2012-08-22-06-00-19 var/
    2012-08-22-06-00-19 usr/
    2012-08-22-06-00-19 tmp/
    2012-08-22-06-00-19 sys/
    2012-08-22-06-00-19 srv/
    2012-08-22-06-00-19 selinux/
    2012-08-22-06-00-19 sbin/
    2012-08-22-06-00-19 run/
    2012-08-22-06-00-19 root/
    2012-08-22-06-00-19 proc/
    2012-08-22-06-00-19 opt/
    2012-08-22-06-00-19 mnt/
    2012-08-22-06-00-19 media/
    2012-08-22-06-00-19 lost+found/
    2012-08-22-06-00-19 lib64/
    2012-08-22-06-00-19 lib/
    2012-08-22-06-00-19 home/
    2012-08-22-06-00-19 etc/
    2012-08-22-06-00-19 dev/
    2012-08-22-06-00-19 boot/
    2012-08-22-06-00-19 bin/
    2012-08-22-06-00-19 .amandaexclude
    2012-08-22-06-00-19 .
    2012-08-20-13-24-40 vmlinuz
    2012-08-20-13-24-40 initrd.img
     
     
    amrecover> history
    200- Dump history for config "DailySet1" host "some-server-name" disk /
    201- 2012-08-22-06-00-19 4 DailySet1-8:1
    201- 2012-08-22-05-36-40 3 DailySet1-7:1
    201- 2012-08-21-10-13-57 3 DailySet1-6:1
    201- 2012-08-21-09-35-12 2 DailySet1-5:1
    201- 2012-08-21-09-10-12 2 DailySet1-4:1
    201- 2012-08-21-06-58-43 1 DailySet1-3:1
    201- 2012-08-21-06-10-58 1 DailySet1-2:1
    201- 2012-08-20-13-24-40 0 DailySet1-1:1
    200 Dump history for config "DailySet1" host "some-server-name" disk /
     
     
     
    amrecover> setdate 2012-08-20-13-24-40
    200 Working date set to 2012-08-20-13-24-40
     
    amrecover> add etc
    Added dir /etc/ at date 2012-08-20-13-24-40
     
    amrecover> list
    TAPE DailySet1-1:1 LEVEL 0 DATE 2012-08-20-13-24-40
         /etc
     
     
    amrecover> lcd /tmp/etc/
    

    собственно начинаем восстановление:

    
    amrecover> extract
    ./etc/xinetd.d/amanda

    ./etc/xinetd.d/chargen
    ./etc/xinetd.d/daytime
    ./etc/xinetd.d/discard
    ./etc/xinetd.d/echo
    ./etc/xinetd.d/time
    ./etc/xml/catalog
    ./etc/xml/catalog.old
    ./etc/xml/xml-core.xml
    ./etc/xml/xml-core.xml.old
    ./etc/zabbix/zabbix_agentd.conf
    ./etc/zabbix/zabbix_agentd.conf.d/iostat.conf
    ./etc/zabbix/zabbix_agentd.conf.d/lxc.conf
    ...
    amrecover>

все, выходим quit, идем в /local/recover/ смотрим что нам нужно и копируем куда нужно.