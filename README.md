Это мой личный блог. [garacio.ru](http://garacio.ru/)

Тема для него взята [здесь](http://html5up.net/striped/)

Основа для статической генерации Flask + Flask-FlatPages + Frozen-Flask.

    python ./sitebuilder.py

запускает devserver

    python ./sitebuilder.py build

генерирует статический контент в каталоге build.
После генерации запускается сжатие статики.

Для этого необходим установленный в системе nodejs.

### Nginx

    server {

        server_name yourdomainname.tld;

        access_log  /var/log/nginx/access.log;
        error_log  /var/log/nginx/error.log;

        index                     index.html;

        root                      /path/to/blog/build;

        location / {
            try_files $uri $uri/ =404;
        }

        location ~ /\. { access_log off; log_not_found off; deny all; }
        location ~ ~$ { access_log off; log_not_found off; deny all; }
        location = /robots.txt { access_log off; log_not_found off; }
        location = /favicon.ico { access_log off; log_not_found off; }


        location ~* \.(ico|gif|jpe?g|png|flv|pdf|swf|mov|mp3|wmv|ppt)$ {
            access_log              off;
            expires                 30d;
            add_header              Cache-Control  "public";
        }
    }
